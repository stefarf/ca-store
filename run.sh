#!/usr/bin/env bash
docker run -it \
  -v "$PWD/ca":/ca \
  --entrypoint "/bin/ash" \
  ca-store $@
