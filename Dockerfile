FROM frapsoft/openssl
COPY scripts/* /usr/local/bin/
COPY cnf/* /root/
WORKDIR /usr/local/bin
ENTRYPOINT /bin/ash