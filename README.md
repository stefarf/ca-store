# Credit

This ca-store is an implementation of: https://jamielinux.com/docs/openssl-certificate-authority/ + some customization 
for my personal use.

# Scripts

```bash
./build.sh # build docker image
./test.sh  # run shell to test
./run.sh <command> # helper to run ca-store command (see command list)
```

# Command list

```bash
./run.sh ca_list
./run.sh ca_init_dir
./run.sh create_root_key
./run.sh create_root_certificate
./run.sh verify_root_certificate
./run.sh create_intermediate_key
./run.sh create_intermediate_certificate
./run.sh verify_intermediate_certificate
./run.sh verify_intermediate_signature
./run.sh chain_root_intermediate
./run.sh create_host_key domain.com
./run.sh create_host_certificate domain.com
./run.sh verify_host_certificate domain.com
./run.sh verify_host_signature domain.com
./run.sh deploy_certificate domain.com
```